import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false

Vue.use(Vuetify, {
  theme: {
    primary: '#303AA6',
    secondary: '#FFFF2C',
    accent: '#673ab7',
    error: '#b71c1c'
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
