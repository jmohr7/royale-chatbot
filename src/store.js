import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    isLocationVoteOpen: false,
    placementGuessStatus: 'READY',
    guesses: [],
    guessScore: [],
    placementTotals: [],
    voteTotals: [],
    whoGuessed: {},
    whoVoted: {}
  },
  mutations: {
    addGuess (state, guess) {
      state.guesses.push(guess)
    },
    addPlacement (state, place) {
      let locIndex = state.placementTotals.findIndex((val) => val.place === place)
      if (locIndex !== -1) {
        state.placementTotals[locIndex].guesses++
      } else {
        state.placementTotals.push({ place: place, guesses: 1 })
      }
    },
    addScore (state, scoreInfo) {
      let locIndex = state.guessScore.findIndex((val) => val.user === scoreInfo.user)
      if (locIndex !== -1) {
        state.guessScore[locIndex].score += scoreInfo.points
      } else {
        state.guessScore.push({ user: scoreInfo.user, score: scoreInfo.points })
      }
    },
    addVote (state, location) {
      let locIndex = state.voteTotals.findIndex((val) => val.location === location)
      if (locIndex !== -1) {
        state.voteTotals[locIndex].votes++
      } else {
        state.voteTotals.push({ location: location, votes: 1 })
      }
    },
    addWhoGuessed (state, username) {
      state.whoGuessed[username] = true
    },
    addWhoVoted (state, username) {
      state.whoVoted[username] = true
    },
    setUser (state, name) {
      state.user = name
    },
    setIsLocationVoteOpen (state, isOpen) {
      state.isLocationVoteOpen = isOpen
    },
    setPlacementGuessStatus (state, status) {
      state.placementGuessStatus = status
    },
    resetGuesses (state) {
      state.guesses = []
    },
    resetGuessScore (state) {
      state.guessScore = []
    },
    resetPlacementTotals (state) {
      state.placementTotals = []
    },
    resetVoteTotals (state) {
      state.voteTotals = []
    },
    resetWhoGuessed (state) {
      state.whoGuessed = {}
    },
    resetWhoVoted (state) {
      state.whoVoted = {}
    }
  },
  actions: {
    // Add points to a player's score
    addUserScore ({ commit }, scoreInfo) {
      commit('addScore', scoreInfo)
    },
    closeGuessing ({ commit }) {
      commit('setPlacementGuessStatus', 'CLOSED')
    },
    closeVote ({ commit }) {
      commit('setIsLocationVoteOpen', false)
    },
    // Record placement guess from viewer
    guess ({ commit }, guessInfo) {
      commit('addGuess', guessInfo)
      commit('addPlacement', guessInfo.place)
      commit('addWhoGuessed', guessInfo.user)
    },
    setGuessingReady ({ commit }) {
      commit('setPlacementGuessStatus', 'READY')
    },
    startLocationVote ({ commit }) {
      commit('resetVoteTotals')
      commit('resetWhoVoted')
      commit('setIsLocationVoteOpen', true)
    },
    startPlacementGuessing ({ commit }) {
      commit('resetGuesses')
      commit('resetPlacementTotals')
      commit('resetWhoGuessed')
      commit('setPlacementGuessStatus', 'OPEN')
    },
    // Record location vote from viewer
    vote ({ commit }, voteInfo) {
      commit('addVote', voteInfo.location)
      commit('addWhoVoted', voteInfo.user)
    }
  }
})
