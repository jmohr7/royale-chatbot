import store from './store'
import Twitch from 'twitch-js'
import { fortniteDetails } from './fortnite-details'

let commandPrefix = '!'

// List of available commands
let knownCommands = { locvote, lz, place }

// Function called when the "locvote" command received
function locvote (target, context, params) {
  // Check if location vote is open
  if (store.state.isLocationVoteOpen) {
    // Check if user already voted
    if (context.username in store.state.whoVoted) {
      console.log(`* ${context.username} already voted`)
    } else {
      // Check if location included in vote
      if (params.length) {
        const location = params.join(' ')
        // Check if location is a valid fortnite location and add the vote
        if (fortniteDetails.landingZones.indexOf(location) !== -1) {
          store.dispatch('vote', { user: context.username, location: location })
          console.log(`* Recorded vote from ${context.username}`)
        }
      } else { // No location supplied
        console.log(`* No location included in the vote`)
      }
    }
  } else { // Location voting isn't open
    sendMessage(target, context, 'Drop location vote hasn\'t started yet')
  }
}

// Function called when the "lz" command received
function lz (target, context) {
  sendMessage(target, context, fortniteDetails.landingZones[Math.floor(Math.random() * fortniteDetails.landingZones.length)])
}

// Function called when the "place" command received
function place (target, context, params) {
  if (store.state.placementGuessStatus === 'OPEN') {
    // Check if user already guessed
    if (context.username in store.state.whoGuessed) {
      console.log(`* ${context.username} already guessed`)
    } else {
      // Check if one param included in vote
      if (params.length === 1) {
        // Check if guess is valid placement
        const place = parseInt(params[0])
        if (place >= 1 && place <= 100) {
          store.dispatch('guess', { user: context.username, place: place })
          console.log(`* Recorded vote from ${context.username}`)
        }
      } else { // No placement supplied
        console.log('* Invalid guess params length')
      }
    }
  } else { // Placement guessing isn't open
    sendMessage(target, context, 'Placement guessing isn\'t open')
  }
}

// Helper function to send the correct type of message
function sendMessage (target, context, message) {
  if (context['message-type'] === 'whisper') {
    client.whisper(target, message)
  } else {
    client.say(target, message)
  }
}

export const client = new Twitch.Client()

// Register event handlers
client.on('message', onMessageHandler)
client.on('connected', onConnectedHandler)
client.on('disconnected', onDisconnectedHandler)

// Handler called for every message received in the channel
function onMessageHandler (target, context, msg, self) {
  // Ignore messages from this chatbot
  if (self) { return }

  // Check message for command prefix, return if it doesn't have it
  if (msg.substr(0, 1) !== commandPrefix) {
    console.log(`[${target} (${context['message-type']})] ${context.username}: ${msg}`)
    return
  }

  // Split message into words
  const parse = msg.slice(1).split(' ')
  // First word is the command name
  const commandName = parse[0]
  // Additional words are command parameters
  const params = parse.splice(1)

  // Check if command name is in list of known commands
  if (commandName in knownCommands) {
    // Get command function
    const command = knownCommands[commandName]
    // Call command function with supplied parameters
    command(target, context, params)
    console.log(`* Executed ${commandName} command for ${context.username}`)
  } else {
    console.log(`* Unknown command ${commandName} from ${context.username}`)
  }
}

// Called every time the chatbot connects to Twitch
function onConnectedHandler (addr, port) {
  console.log(`* Connected to ${addr}:${port}`)
}

// Called every time the chatbot disconnects from Twitch
function onDisconnectedHandler (reason) {
  console.log(`Disconnected: ${reason}`)
}
