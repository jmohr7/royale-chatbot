import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import Login from './views/Login.vue'
import store from './store'
import { client } from './chat'

Vue.use(Router)

// Route guard to only allow logged in users
function guardRouteLoggedIn (to, from, next) {
  if (store.state.user) {
    next()
  } else if (localStorage.details) {
    const details = JSON.parse(localStorage.details)
    // Define configuration options:
    let opts = {
      options: {
        commandTimeout: 600
      },
      connection: {
        reconnect: true,
        port: 443,
        secure: true
      },
      identity: {
        username: details.username,
        password: details.token
      },
      channels: [
        details.channel
      ]
    }
    client.opts = opts
    client.connect().then(() => {
      store.commit('setUser', details.username)
      next()
    }).catch((err) => {
      console.log(`Login ailed: ${err}`)
      next('/login')
    })
  } else {
    next('/login')
  }
}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'root',
      component: Dashboard,
      beforeEnter: guardRouteLoggedIn
    },
    {
      path: '/index.html',
      name: 'index',
      component: Dashboard,
      beforeEnter: guardRouteLoggedIn
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      beforeEnter: guardRouteLoggedIn
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/logout',
      beforeEnter (to, from, next) {
        client.disconnect().then(() => {
          localStorage.removeItem('details')
          store.commit('setUser', null)
          next('/login')
        }).catch(
          (err) => {
            console.log('Logout Failed: ', err)
          }
        )
      }
    },
    {
      path: '*',
      name: 'not-found',
      component: () => import(/* webpackChunkName: "about" */ './views/NotFound.vue')
    }
  ]
})
