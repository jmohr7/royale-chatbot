export const fortniteDetails = {
  landingZones: [
    'Dusty Divot',
    'Fatal Fields',
    'Flush Factory',
    'Greasy Grove',
    'Haunted Hills',
    'Junk Junction',
    'Lazy Links',
    'Lonely Lodge',
    'Loot Lake',
    'Lucky Landing',
    'Paradise Palms',
    'Pleasant Park',
    'Retail Row',
    'Risky Reels',
    'Salty Springs',
    'Shifty Shafts',
    'Snobby Shores',
    'Tilted Towers',
    'Tomato Temple',
    'Wailing Woods'
  ]
}
