# Royale Chatbot

Royale Chatbot is a Twitch chatbot for increasing viewer engagment while streaming Fortnite.  It is
a Progressive Web Application (PWA) built with Vue.js and can be installed on a mobile device like a native app.

## Live Demo

A live demo of the application can be found at https://royale.josephpmohr.com

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

## License
MIT
