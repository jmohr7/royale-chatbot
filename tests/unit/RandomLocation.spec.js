import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import RandomLocation from '@/components/RandomLocation.vue'

describe('RandomLocation.vue', () => {
  it('renders name of location', () => {
    const location = 'Dusty Divot'
    const wrapper = shallowMount(RandomLocation)
    wrapper.setData({ location: location })
    expect(wrapper.text()).to.include(location)
  })
})
