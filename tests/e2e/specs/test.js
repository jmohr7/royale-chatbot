// https://docs.cypress.io/api/introduction/api.html

describe('Royale Chatbot', () => {
  it('app name in main toolbar', () => {
    cy.visit('/')
    cy.contains('#toolbarTitle', 'Royale Chatbot')
  })
})
